///////////////////////////////////////////////////////////////////////////////
/////////////  The Header File for the Buffer Manager /////////////////////////
///////////////////////////////////////////////////////////////////////////////


#ifndef BUF_H
#define BUF_H

#include "db.h"
#include "page.h"
#include <list>
#include <algorithm>

#define NUMBUF 20   
// Default number of frames, artifically small number for ease of debugging.

#define HTSIZE 7
// Hash Table size
//You should define the necessary classes and data structures for the hash table, 
// and the queues for LSR, MRU, etc.

#define A 3
#define B 1 

/*******************ALL BELOW are purely local to buffer Manager********/

// You should create enums for internal errors in the buffer manager.
enum bufErrCodes  { 
    MISSING_PAGE, BUFFER_SIZE_EXCEEDED, INVALID_UNPIN, FREEING_PINNED_PAGE
};

class Replacer;

class BufMgr {

private: // fill in this area
    std::list<PageId> freeList;
    int bufferNum;
    
public:

    Page* bufPool; // The actual buffer pool

    struct HashNode {
        PageId pageNum;
        int pageIndex;
        HashNode *next;
        HashNode(PageId p, int i, HashNode *n = 0) :
            pageNum(p), pageIndex(i), next(n) {}
    }*hash[HTSIZE];

    struct ListNode {
        int index;
        ListNode *prev;
        ListNode *next;
    } *hListHead, *hListTail, *lListHead, *lListTail;

    void list_erase(ListNode *p) {
        ListNode *prev = p -> prev;
        ListNode *next = p -> next;
        prev -> next = next;
        next -> prev = prev;
        delete p;
    }

    int list_pop(ListNode *p) {
        int ret = p -> next -> index;
        list_erase(p -> next);
        return ret;
    }

    ListNode *list_push(int index, ListNode *p, ListNode *q) {
        ListNode *node = new ListNode;
		node -> index = index;
        node -> prev = p;
        node -> next = q;
        if (p) {
            p -> next = node;
        }
        if (q) {
            q -> prev = node;
        }
        return node;
    }

    struct Descriptor {
        PageId pageNum;
        int pinCount;
        int dirty;
        int valid;
		int hated;
        ListNode *node;
        Descriptor(PageId n = 0, int v = 0, int c = 1, int d = 0, int h = 1) :
            pageNum(n), pinCount(c), dirty(d), valid(v), hated(h), node(0) {}
    } *bufDescr;

    int h(PageId value) {
        return (A * value + B) % HTSIZE;
    }

    int hash_find(PageId id) {
        int hv = h(id);
        HashNode *p = hash[hv];
        while (p) {
            if (p -> pageNum == id) {
                return p -> pageIndex;
            }
            p = p -> next;
        }
        return -1;
    }

    int hash_remove(PageId id) {
        int hv = h(id);
        HashNode *p = hash[hv], *q = 0;
        while (p) {
            if (p -> pageNum == id) {
                if (q) {
                    q -> next = q -> next -> next;
                }
                else {
                    hash[hv] = hash[hv] -> next; 
                }
                delete p;
                return 1;
            }
            q = p;
            p = p -> next;
        }
        return 0;
    }
    
    BufMgr (int numbuf, Replacer *replacer = 0); 
    // Initializes a buffer manager managing "numbuf" buffers.
	// Disregard the "replacer" parameter for now. In the full 
  	// implementation of minibase, it is a pointer to an object
	// representing one of several buffer pool replacement schemes.

    ~BufMgr();           // Flush all valid dirty pages to disk

    Status pinPage(PageId PageId_in_a_DB, Page*& page, int emptyPage=0);
        // Check if this page is in buffer pool, otherwise
        // find a frame for this page, read in and pin it.
        // also write out the old page if it's dirty before reading
        // if emptyPage==TRUE, then actually no read is done to bring
        // the page

    Status unpinPage(PageId globalPageId_in_a_DB, int dirty, int hate);
        // hate should be TRUE if the page is hated and FALSE otherwise
        // if pincount>0, decrement it and if it becomes zero,
        // put it in a group of replacement candidates.
        // if pincount=0 before this call, return error.

    Status newPage(PageId& firstPageId, Page*& firstpage, int howmany=1); 
        // call DB object to allocate a run of new pages and 
        // find a frame in the buffer pool for the first page
        // and pin it. If buffer is full, ask DB to deallocate 
        // all these pages and return error

    Status freePage(PageId globalPageId); 
        // user should call this method if it needs to delete a page
        // this routine will call DB to deallocate the page 

    Status flushPage(PageId pageid);
        // Used to flush a particular page of the buffer pool to disk
        // Should call the write_page method of the DB class

    Status flushAllPages();
	// Flush all pages of the buffer pool to disk, as per flushPage.

    /* DO NOT REMOVE THIS METHOD */    
    Status unpinPage(PageId globalPageId_in_a_DB, int dirty=FALSE)
        //for backward compatibility with the libraries
    {
      return unpinPage(globalPageId_in_a_DB, dirty, FALSE);
    }
};

#undef A
#undef B

#endif
